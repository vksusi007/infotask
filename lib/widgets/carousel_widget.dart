import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:info_home/models/service_model.dart';

class CarouselWithIndicator extends StatefulWidget {
  List<Carousel> sliderList;
  bool autoPlay;
  bool enlargeCenterPage;
  double aspectRatio;
  double radius;
  double margin;

  CarouselWithIndicator(
      {this.sliderList,
      this.aspectRatio,
      this.autoPlay,
      this.enlargeCenterPage,
      this.radius,
      this.margin});

  @override
  _CarouselWithIndicatorState createState() => _CarouselWithIndicatorState();
}

class _CarouselWithIndicatorState extends State<CarouselWithIndicator> {
  int _current = 0;
  List child;

  @override
  void initState() {
    super.initState();
    print(widget.sliderList.length);
    initSlider();
  }

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }

    return result;
  }

  void initSlider() {
    child = map<Widget>(
      widget.sliderList,
      (int index, Carousel i) {
        return Container(
          margin: EdgeInsets.all(widget.margin ?? 5.0),
          child: ClipRRect(
            borderRadius:
                BorderRadius.all(Radius.circular(widget.radius ?? 5.0)),
            child: Image.network(
              i.image,
              fit: BoxFit.cover,
              width: 1000.0,
            ),
          ),
        );
      },
    ).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      CarouselSlider(
        items: child,
        options: CarouselOptions(
            autoPlay: widget.autoPlay ?? true,
            enlargeCenterPage: widget.enlargeCenterPage ?? true,
            aspectRatio: widget.aspectRatio ?? 2.0,
            onPageChanged: _pageChanging),
      ),
      new Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height / 4.2),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: map<Widget>(
            widget.sliderList,
            (index, url) {
              return Container(
                width: 8.0,
                height: 8.0,
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _current == index ? Colors.pink : Colors.grey[200]),
              );
            },
          ),
        ),
      ),
    ]);
  }

  carouselmethod() {
    setState(() {});
  }

  _pageChanging(int index, CarouselPageChangedReason reason) {
    setState(() {
      _current = index;
    });
  }
}
