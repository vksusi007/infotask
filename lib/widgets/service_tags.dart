import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:info_home/models/service_model.dart';

class ServiceTag extends StatefulWidget {
  List<Services> tagServices;
  Function changeTag;
  Map<String, dynamic> keyMap;
  ServiceTag({this.tagServices, this.changeTag, this.keyMap}) : super();
  @override
  ServiceTagState createState() => ServiceTagState();
}

class ServiceTagState extends State<ServiceTag> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: widget.tagServices
              .asMap()
              .map((index, eachService) => MapEntry(
                  index,
                  InkWell(
                    child: Container(
                      padding: EdgeInsets.all(8.0),
                      margin: EdgeInsets.only(left: 10.0),
                      decoration: BoxDecoration(
                        color: widget.keyMap
                                .containsValue(eachService.serviceId.toString())
                            ? Colors.pink
                            : Colors.white,
                        border: Border.all(
                            color: widget.keyMap.containsValue(
                                    eachService.serviceId.toString())
                                ? Colors.pink
                                : Colors.grey),
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      child: Text(
                        eachService.serviceName,
                        style: TextStyle(
                            color: widget.keyMap.containsValue(
                                    eachService.serviceId.toString())
                                ? Colors.white
                                : Colors.grey),
                      ),
                    ),
                    onTap: () {
                      print(eachService.serviceId);
                      setState(() {
                        widget.keyMap.update(
                            "id", (value) => eachService.serviceId.toString());
                      });
                      print(widget.keyMap
                          .containsValue(eachService.serviceId.toString()));
                      widget.changeTag(
                          eachService.serviceId, eachService.serviceName);
                    },
                  )))
              .values
              .toList(),
        ),
      ),
    );
  }
}
