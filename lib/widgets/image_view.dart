import 'package:flutter/cupertino.dart';

class ImageView extends StatelessWidget {
  String imagePath;
  ImageView({this.imagePath}) : super();
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(5.0),
        child: Image.network(
          imagePath,
          height: 100,
          width: 150,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
