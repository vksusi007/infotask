import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:info_home/bloc/service_bloc.dart';
import 'package:info_home/models/service_list_model.dart';
import 'package:info_home/models/service_model.dart';
import 'package:info_home/services/api_services.dart';
import 'package:info_home/services/constants.dart' as global;
import 'package:info_home/widgets/carousel_widget.dart';
import 'package:info_home/widgets/image_view.dart';
import 'package:info_home/widgets/service_tags.dart';

class HomePage extends StatefulWidget {
  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  ServiceBloc _serviceBloc = ServiceBloc();
  ScrollController _scrollController = new ScrollController();
  List<Carousel> sliderItems = [];
  List<Services> serviceItems = [];
  List<Spa> subServiceList = [];
  int serviceId = 1;
  int selectedServiceId = 1;
  int page = 1;
  int limit = 5;
  String title = "";
  String appBarTitle = "Top Categories";
  Map<String, dynamic> keyMap = Map();
  RegExp _regExp = new RegExp(r'([0-9]*[.])[0-9]+');
  List _scrollOffsetCount = ['0.0'];
  final _dataKey = new GlobalKey();




  @override
  void initState() {
    super.initState();
    keyMap.putIfAbsent("id", () => "1");
    initialFetch(true, limit, serviceId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
            appBarTitle,
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
          elevation: 0.0,
          centerTitle: true,
          leading: IconButton(
            icon: Icon(
              Icons.menu,
              color: Colors.black,
            ),
            onPressed: () {},
          ),
          actions: [
            Icon(
              Icons.search,
              color: Colors.black,
            )
          ],
        ),
        body: sliderItems.length == 0 || serviceItems.length == 0
            ? Center(child: CircularProgressIndicator())
            : Column(
                children: [
                  CarouselWithIndicator(
                    sliderList: sliderItems,
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  ServiceTag(
                    tagServices: serviceItems,
                    changeTag: changeServiceTag,
                    keyMap: keyMap,
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          title,
                          style: TextStyle(
                              color: Colors.grey, fontWeight: FontWeight.bold),
                        ),
                        Row(
                          children: [
                            Text(
                              "Filter",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              width: 5.0,
                            ),
                            Icon(
                              Icons.filter_alt_sharp,
                              color: Colors.black,
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    child: StreamBuilder(
                      stream: _serviceBloc.servicePipe$,
                      builder: (context,
                          AsyncSnapshot<Map<String, dynamic>> snapshot) {
                        if (!snapshot.hasData)
                          return Center(child: CircularProgressIndicator());
                        subServiceList = snapshot.data[global.serviceList];
                        return ListView.builder(
                            key: _dataKey,
                            controller: _scrollController,
                            itemCount: subServiceList.length,
                            padding: EdgeInsets.only(left: 5.0, right: 5.0),
                            itemBuilder: (context, index) {
                              if (_dataKey.currentContext.toString() != null &&_scrollController
                                          .position.userScrollDirection ==
                                      ScrollDirection.reverse) {
                                print("Pagination will be called");
                                callInfiniteLoop(_dataKey.currentContext.toString());
                                // limit = limit + 5;
                                // initialFetch(false, limit, serviceId);
                              }
                              return
                                  subServiceList[index].serviceId ==
                                          selectedServiceId.toString()
                                      ? Stack(
                                        children: [
                                          Card(
                                              child: Row(
                                                children: [
                                                  ImageView(
                                                    imagePath: subServiceList[index]
                                                        .spaImage,
                                                  ),
                                                  SizedBox(
                                                    width: 10.0,
                                                  ),
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.start,
                                                    children: [
                                                      Text(
                                                        subServiceList[index]
                                                            .spaName,
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold),
                                                      ),
                                                      SizedBox(
                                                        height: 10.0,
                                                      ),
                                                      Text(
                                                        "${subServiceList[index].openTime} ${subServiceList[index].openMeridiem} - "
                                                        "${subServiceList[index].closeTime} ${subServiceList[index].closeMeridiem}",
                                                        style: TextStyle(
                                                            color: Colors.green),
                                                      ),
                                                      SizedBox(
                                                        height: 10.0,
                                                      ),
                                                      Row(
                                                        children: [
                                                          Icon(
                                                            Icons.location_on,
                                                            color: Colors.black,
                                                          ),
                                                          SizedBox(
                                                            width: 5.0,
                                                          ),
                                                          Text(
                                                              "${subServiceList[index].distance} ${subServiceList[index].distanceUnit}")
                                                        ],
                                                      )
                                                    ],
                                                  )
                                                ],
                                              ),
                                            ),
                                          subServiceList[index].bookAvailable =="0"?
                                          Positioned(
                                            right: 0.0,
                                            bottom: 5.0,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  color: Colors.pink,
                                                  borderRadius: BorderRadius.circular(5.0)
                                              ),
                                              padding: EdgeInsets.all(8.0),
                                              margin: EdgeInsets.only(right: 5.0),
                                              child: Text("  Book  ", style: TextStyle(color: Colors.white),),
                                            ),
                                          ): Container(),
                                        ],
                                      )
                                      : Container();
                            });
                      },
                    ),
                  ),
                ],
              ));
  }

  void initialFetch(bool callGetService, limitNo, serviceIdNo) async {
    if (callGetService) {
      var result = await ApiService().getServices();
      if (result is ServiceModel) {
        setState(() {
          serviceItems.addAll(result.services);
          sliderItems.addAll(result.carousel);
          title = serviceItems[0].serviceName;
        });
      }
    }
    var listResult = await _serviceBloc.fetchListOfServices(page.toString(),
        limitNo.toString(), serviceIdNo.toString(), serviceIdNo == serviceId);
  }

  changeServiceTag(int serviceIds, String serviceName) {
    setState(() {
      title = serviceName;
      appBarTitle = serviceName;
      limit = 5;
      selectedServiceId = serviceIds;
      //serviceId = serviceIds;
      keyMap.update("id", (value) => serviceIds.toString());
    });
    initialFetch(false, limit, serviceIds);
  }

  void callInfiniteLoop(listViewElement)
  {
    if (_regExp.hasMatch(listViewElement)) if (!_scrollOffsetCount
        .contains(_regExp.stringMatch(listViewElement)))
      {
        int feedLengthHalf = _serviceBloc.feedLength ~/ 2;
        if (_scrollOffsetCount.length - 1 == _serviceBloc.feedLength) {
          _scrollOffsetCount.clear();
        }
        _scrollOffsetCount.add(_regExp.stringMatch(listViewElement));
        if(feedLengthHalf == _scrollOffsetCount.length - 1)
          {
             limit = limit+5;
             initialFetch(false, limit, selectedServiceId);
          }
      }
  }
}
