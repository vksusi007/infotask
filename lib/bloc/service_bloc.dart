import 'package:info_home/models/service_list_model.dart';
import 'package:info_home/models/service_model.dart';
import 'package:info_home/services/api_services.dart';
import 'package:info_home/services/constants.dart' as global;
import 'package:rxdart/rxdart.dart';

class ServiceBloc {
  List<Carousel> carouselList = [];
  List<Services> servicesList = [];
  List<Spa> subServiceList = [];
  int feedLength = 0;
  //feed subscribe
  final _serviceData = BehaviorSubject<Map<String, dynamic>>();
  //listen feed
  Stream<Map<String, dynamic>> get servicePipe$ =>
      _serviceData.stream.asBroadcastStream();
  //update feed
  Sink<Map<String, dynamic>> get serviceResult => _serviceData.sink;

  fetchServices() async {
    var response = await ApiService().getServices();
    if (response != null && response is ServiceModel) {
      carouselList.addAll(response.carousel);
      servicesList.addAll(response.services);
      try {
        serviceResult.add(
            {global.carousel: carouselList, global.services: servicesList});
      } catch (err) {
        print(err);
      }
    }
  }

  fetchListOfServices(String pageNo, String limit, String serviceId,
      bool isServiceChange) async {
    var response = await ApiService().getServiceList(serviceId, pageNo, limit);
    if (response is ServiceListModel) {
      if (isServiceChange) {
        subServiceList.clear();
      }
      subServiceList.addAll(response.spa);
      feedLength = subServiceList.length;
      try {
        serviceResult.add({global.serviceList: subServiceList});
      } catch (err) {
        print(err);
      }
    }
  }
}
