class ServiceListModel {
  bool status;
  List<Spa> spa;

  ServiceListModel({this.status, this.spa});

  ServiceListModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['spa'] != null) {
      spa = new List<Spa>();
      json['spa'].forEach((v) {
        spa.add(new Spa.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.spa != null) {
      data['spa'] = this.spa.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Spa {
  int id;
  String serviceId;
  String spaImage;
  String spaName;
  String openTime;
  String openMeridiem;
  String closeTime;
  String closeMeridiem;
  String distance;
  String distanceUnit;
  String bookAvailable;
  String message;

  Spa(
      {this.id,
      this.serviceId,
      this.spaImage,
      this.spaName,
      this.openTime,
      this.openMeridiem,
      this.closeTime,
      this.closeMeridiem,
      this.distance,
      this.distanceUnit,
      this.bookAvailable,
      this.message});

  Spa.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    serviceId = json['serviceId'];
    spaImage = json['spaImage'];
    spaName = json['spaName'];
    openTime = json['openTime'];
    openMeridiem = json['openMeridiem'];
    closeTime = json['closeTime'];
    closeMeridiem = json['closeMeridiem'];
    distance = json['distance'];
    distanceUnit = json['distanceUnit'];
    bookAvailable = json['bookAvailable'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['serviceId'] = this.serviceId;
    data['spaImage'] = this.spaImage;
    data['spaName'] = this.spaName;
    data['openTime'] = this.openTime;
    data['openMeridiem'] = this.openMeridiem;
    data['closeTime'] = this.closeTime;
    data['closeMeridiem'] = this.closeMeridiem;
    data['distance'] = this.distance;
    data['distanceUnit'] = this.distanceUnit;
    data['bookAvailable'] = this.bookAvailable;
    data['message'] = this.message;
    return data;
  }
}
