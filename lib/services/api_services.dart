import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:info_home/models/service_list_model.dart';
import 'package:info_home/models/service_model.dart';

class ApiService {
  final String baseUrl = "https://admin.blissme.hk/public/api/";
  var headers = {
    HttpHeaders.acceptHeader: "application/json",
    "Authorization":
        "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiZjk3NmU4NWM5MTJhNjhlMTQwNDcyMmJhZTg4MTM5MWU3NTc0MjM0ZjU4MDM3YTY1NDcwNDQ2NDJjMDc0ZGUwZDlmMjA1NDk0ZDllZjU5ZWMiLCJpYXQiOjE2MjM5MTIxMDIsIm5iZiI6MTYyMzkxMjEwMiwiZXhwIjoxNjU1NDQ4MTAyLCJzdWIiOiI3MiIsInNjb3BlcyI6W119.OUlaYTOS-y5Y2rtMKzcpt10Ps2zDWZnt3KMtY1_6nL9qnn9iWXiN6roqkYLDsAf1s-aWzVX3ZfsVuIxSCMlwcp4_VRVu_J4VKo-PY8pUCeaHr4QpUs9l2UhySJTLhQJGyIwo2MNI7EW_Mmx45R8iisyvwQf4yegcJ9Wq-vTQ6WhmPhQfwQYYwNPxCrp8F9vEgCXcSQth8LT9pRvVmDW8P9UPkelMnVIVLIJEtw6qtsBl1Cr3uDvxDRJKq3HDZU_Rm6Ir8iC8s0QLmKxRubMHA0UdRoeTmxp2T8kb4xH-icMzQjXayuiSY6cJWws8IP7vF-DyYYdYMubJ4peq5ER-ZcHVRV-CEcAv2sCGHG-tX-uAo5SoQrl8Ebk66vjTu-emfBbTSIDWMdg09nmqOvh_AHI2lkVFauUTUMaIKqbdYLZqgC6JHD2D_OFmn2sRNw7MzpIGCwFcXuZnCN9trfWgQv4e4xnooa9QX-Yj8-3stl3u_ehbgWnFc-ZqHSfQhusJUed9S8mEGqhhPmdZTyPu66gWpuZU_p3hPJ3uPXl84WOrJgyEMDOgy3VHm5RyrC5zbAGb3KtRNnyyfJdUaqpxCRDrhDV6R6Gsaurvp1VazC8WyjvAZm309O62do6bCixpazipnfRBiv3scY6pqK2nUbNY_kCwVw7TxvMJsjkJ06g"
  };

  getServices() async {
    var url = Uri.parse(baseUrl + "services");
    var response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      var decodeRes = jsonDecode(response.body);
      return ServiceModel.fromJson(decodeRes);
    } else {
      return response?.reasonPhrase;
    }
  }

  getServiceList(String serviceId, String page, String limit) async {
    var url = Uri.parse(
        baseUrl + "servicelist?serviceId=$serviceId&page=$page&limit=$limit");
    var response = await http.post(url, headers: headers);
    if (response.statusCode == 200) {
      var decodeResponse = jsonDecode(response.body);
      return ServiceListModel.fromJson(decodeResponse);
    } else {
      return response?.reasonPhrase;
    }
  }
}
